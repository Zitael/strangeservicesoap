package ru.alexandrov.soap;

import ru.alexandrov.model.Goods;

@javax.jws.WebService(
        endpointInterface = "ru.alexandrov.soap.WebService",
        serviceName = "HelloSoap")
public class HelloSoap implements WebService {
    public String testService() {
        return "Hello from SOAP Webservice!";
    }
    public String sayHelloTo(String text) {
        return "Hello to " + text;
    }

    public Goods getGoods() {
        Goods goods = new Goods();
        goods.setId(1);
        goods.setName("Some goods test name");
        return goods;
    }
}
