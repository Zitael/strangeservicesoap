package ru.alexandrov.soap;

import ru.alexandrov.model.Goods;

import javax.jws.WebMethod;
import javax.jws.WebParam;

@javax.jws.WebService
public interface WebService {
    @WebMethod//annotation optional and is mainly used to provide a name attribute to the public method in wsdl
    String testService();

    @WebMethod
    String sayHelloTo(@WebParam(name = "text") String text);

    @WebMethod
    Goods getGoods();
}
