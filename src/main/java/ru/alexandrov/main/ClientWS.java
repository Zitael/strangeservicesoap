package ru.alexandrov.main;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import ru.alexandrov.model.Goods;
import ru.alexandrov.soap.WebService;

public class ClientWS {

    public static void main(String[] args) {
        testSOAPFromClient();
    }

    /**
     * create client and test soap service
     */
    private static void testSOAPFromClient() {
        String soapServiceUrl = "http://localhost:8080/soap/webservice";

        JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
        factoryBean.setServiceClass(WebService.class);
        factoryBean.setAddress(soapServiceUrl);

        WebService webservice = (WebService) factoryBean.create();

        Goods result = webservice.getGoods();
        System.out.println("Result: " + result);
    }

}
