package ru.alexandrov.try2;

import ru.alexandrov.try2.MyService;

import javax.xml.ws.Endpoint;

public class ServiceStarter {
    public static void main(String[] args) {
        String url = "http://localhost:8080/hello";
        Endpoint.publish(url, new MyService());
        System.out.println("Service started @ " + url);

    }
}
