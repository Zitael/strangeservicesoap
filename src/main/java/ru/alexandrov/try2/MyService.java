package ru.alexandrov.try2;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public class MyService {
    public String sayHello(String name) {
        return "Hello " + name;
    }
}
