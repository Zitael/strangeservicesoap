package ru.alexandrov.try1.endpoint;

import ru.alexandrov.try1.ws.EndpointInterfaceImpl;

import javax.xml.ws.Endpoint;

public class EndpointPublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9999/ws/hello", new EndpointInterfaceImpl());
    }
}
