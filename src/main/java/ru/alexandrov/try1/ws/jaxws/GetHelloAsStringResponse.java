package ru.alexandrov.try1.ws.jaxws;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "getHelloWorldAsStringResponse", namespace = "http://ws.alexandrov.ru/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getHelloWorldAsStringResponse", namespace = "http://ws.alexandrov.ru/")
public class GetHelloAsStringResponse {
    @XmlElement(name = "return", namespace = "")
    private String _return;

    /**
     *
     * @return
     *     returns String
     */
    public String getReturn() {
        return this._return;
    }

    /**
     *
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(String _return) {
        this._return = _return;
    }
}
