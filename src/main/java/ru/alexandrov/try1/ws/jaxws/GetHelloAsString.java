package ru.alexandrov.try1.ws.jaxws;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "getHelloAsString", namespace = "http://ws.alexandrov.ru/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getHelloAsString", namespace = "http://ws.alexandrov.ru/")
public class GetHelloAsString {
    @XmlElement(name = "arg0", namespace = "")
    private String arg0;

    /**
     *
     * @return
     *     returns String
     */
    public String getArg0() {
        return this.arg0;
    }

    /**
     *
     * @param arg0
     *     the value for the arg0 property
     */
    public void setArg0(String arg0) {
        this.arg0 = arg0;
    }
}
