package ru.alexandrov.try1.ws;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.alexandrov.try1.ws.EndpointInterface")
public class EndpointInterfaceImpl implements EndpointInterface {
        public String getHelloAsString(String name) {
            return "Hello, " + name;
        }
}
